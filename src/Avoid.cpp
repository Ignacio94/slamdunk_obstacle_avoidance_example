/*
 * Copyright (c) 2016 Parrot S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the Parrot Company nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE PARROT COMPANY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "episkopi/Avoid.hpp"

#include "episkopi/AvoidSettings.hpp"

#include <Eigen/Geometry>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>

//////PASOS A SEGUIR:::
/// 1. Determinar si el obtaculpo esta en la direccion que te mueves y sus celdas adyacentes (en función del tamaño del dron y de la division de celdas) //ECHOOO PERO FALTA COMPROBAR QUE EL RESULTADO ES EL CORRECTO
/// 2. Determinar si hay obtaculo en la direccion //ECHOOO PERO FALTA COMPROBAR QUE EL RESULTADO ES EL CORRECTO /// Comprobar la direccion en la que vas modificado
/// 3. UNa vez detectado buscar macroceldas que quedan libres y de cuantas celdas son // COMPLETADOOOO
/// 4.escoger la macro celda mas grande y que sea suficiente para que quepa el dron  // COMPLETADOOOO // quiza coger el mas cercano a la direccion que voy
/// 5. Determinar el centro de esta  // COMPLETADOOOO
/// 6. Tener en cuenta que si existen dos macro celdas muy parecidad que de una iteracción a otra mantenga la dirección en la que iba ///*COMPLETOOO
///
///

namespace episkopi
{
//Recibir la posicion que queremos ir
//void Avoid::obstacleavoidance(std::array<float, 3> position, std::array<float, 3> orientation, const cv::Mat1f &img)
Eigen::Vector3f  Avoid::obstacleavoidance(const cv::Mat1f &img,ros::NodeHandle ros_node,std::array<float, 3> speedref,std::array<float, 3> pose, Mat3x3 Rot_body_to_world)
{   

    pub_reference_speed_world = ros_node.advertise<slamdunk_obstacle_avoidance_example::droneSpeeds>("/drone5/droneSpeedsRefs", 1);
   // cv::Mat img_2;
    //cv::resize(img,img_2,cv::Size(96,96));
    //cv::imshow("13333",img_2);
    // Setup a rectangle to define your region of interest
    cv::Rect myROI(0, CROPSTARTHEIGHT, img.cols, CROPHEIGHT);

    // Crop the full image to that image contained by the rectangle myROI
    cv::Mat croppedImage = img(myROI);
    cv::imshow("12121",croppedImage);
    cv::waitKey(1);
    obstacle=0;
    direction=0;
    int windowstocheck;
    float meterwindows;
    int firstwin;
    int lastwin;
    // number of windows es en cuantas ventanas quieres dividir
    // index sale el numero de pixeles por ventana
    index=croppedImage.cols/NUMBERWINDOWS;
    // recorres cada ventana
    for (unsigned short j = 0; j <NUMBERWINDOWS ; j++){

        buscar(0,j*index,croppedImage.rows-1,index*(j+1)-1,croppedImage,j,speedref);



    }

        std::cout<<atan2(speedref[1],speedref[0])<<std::endl;
//        if (atan2(speedref[1],speedref[0])<-0.7) && (atan2(speedref[1],speedref[0])>0.7){

//        }
        //calculas cuantos metros son cada windows de anchura
        if (direction==0 && atan2(speedref[1],speedref[0])<((NUMBERWINDOWS/2-1)*(atan2(3.4/NUMBERWINDOWS,1.7)))){
              direction=floor((NUMBERWINDOWS/2)-((atan2(speedref[1],speedref[0]))/(atan2(3.4/NUMBERWINDOWS,1.7))));
        }
        std::cout<<"direccion"<<direction<<std::endl;
        meterwindows=valor[direction]*(STANDARD_WIDTH/STANDARD_DISTANCE)/NUMBERWINDOWS;
        if (valor[direction]==0){
            meterwindows=0.12;
        }
        //std::cout<<meterwindows<<std::endl;
        //determinas cuantas windows comprobar para salvar la anchura del dron
        windowstocheck=ceil(((DRONE_WIDTH/meterwindows)-1)/2);
        std::cout<<windowstocheck<<std::endl;
        // Calculas que celdas mirar
        firstwin= direction-windowstocheck;
        lastwin=direction+windowstocheck;
        if (firstwin<0){
            lastwin=lastwin+abs(firstwin);
            firstwin=0;
        }
        if (lastwin>NUMBERWINDOWS-1){
            firstwin=firstwin-(lastwin-NUMBERWINDOWS-1);
            lastwin=NUMBERWINDOWS-1;
        }
        //compruebas las ventanas de alrededor y la propia si el porcentaje esta por encima del 35%
        for (unsigned short j =firstwin ; j <=lastwin ; j++){
            if (porcentaje[j] >35){



                //activas la variable diciendo que hay obstaculo
                if (direction>NUMBERWINDOWS || direction<0){
                obstacle=0;
                }else{
                obstacle=1;
                break;
                }
                //std::cout<<"windows"<<windows<<std::endl;

                //std::cout<< "OBSTACLE WINDOWS :"<< j  << "Porxcentaje"<< porcentaje[j] <<std::endl;

            }
        }
    //std::cout<<"2"<<std::endl;
    // si hay obtaculo es que esta en la direccion que te mueves
    ////SEGUIR AQUI CON EL PUNTO TRES UNA VEZ QUE ENTRA  BUSCAR MACROCELDAS LIBRES
        if (obstacle==1){
            //std::cout<<"POrcentaje"<<porcentaje<<std::endl;
            // compruebo cuantas macroceldas hay libres y cuantas ventanas tiene cada una.
            int order=1;
            int numbercellfree=0;
            int endwindows=0;
            // std::cout<<"3"<<std::endl;
            for (unsigned short j = 0; j <NUMBERWINDOWS ; j++){
                //std::cout<<porcentaje.size()<<std::endl;
                if (porcentaje[j]>35){
                    if (endwindows==1){
                        //std::cout<<"1"<<std::endl;
                        celdasorder.push_back(0);
                        macrocell.push_back(numbercellfree);
                        order=order+1;
                        numbercellfree=0;
                        endwindows=0;
                    }
                    else{
                        // std::cout<<"2"<<std::endl;
                        celdasorder.push_back(0);
                    }
                }
                else{
                    //std::cout<<"3"<<std::endl;
                    endwindows=1;
                    celdasorder.push_back(order);
                    numbercellfree=numbercellfree+1;
                    if (j==NUMBERWINDOWS-1){
                        macrocell.push_back(numbercellfree);
                    }
                }
                std::cout<<celdasorder[j] <<std::endl;

            }

            //std::cout<<"4"<<std::endl;
            // std::cout<<macrocell.size()<<std::endl;
            // std::cout<<celdasorder.size()<<std::endl;
            int centro=0;
            if (macrocell.empty()){
                centro=0;
            }else{
                //Elimino las macroceldas que sean menores a la anchura del dron
                for (unsigned short j = 0; j <macrocell.size() ; j++){
                    //std::cout<<macrocell[j]<<std::endl;
                    //std::cout<<"5"<<std::endl;
                    if (macrocell[j]<windowstocheck*2){
                        for (unsigned short i = 0; i <celdasorder.size() ; i++){

                            if (celdasorder[i]==j+1){
                                celdasorder[i]=0;
                            }
                            else{
                                if (celdasorder[i]>j+1){
                                    celdasorder[i]=celdasorder[i]-1;
                                }
                            }
                        }
                        eliminar.push_back(j);
                        //std::cout<<j <<std::endl;
                    }


                }
                //std::cout<<"4"<<std::endl;
                int eliminado=0;
                for (unsigned short j = 0; j <eliminar.size() ; j++){
                    macrocell.erase(macrocell.begin()+eliminar[j]-eliminado);
                    eliminado=eliminado+1;
                }
                if (macrocell.empty()){
                    centro=0;
                }else{
                    // cojo la macrocelda con mayor de ventans vacias y busco la ventana central
                    //              int cellnumber=std::distance(macrocell.begin(),std::max_element(macrocell.begin(),macrocell.end()));
                    //              std::cout<<cellnumber<<std::endl;
                    //              for (unsigned short i = 0; i <celdasorder.size() ; i++){

                    //                  if (celdasorder[i]==cellnumber+1){
                    //                      centro=i+floor(macrocell[cellnumber]/2);
                    //                      break;
                    //                  }
                    //              }
                    //              }


                    //COJO la macrocelda que mas cercana esta a la direction que me dirijo.
                    int m=1;
                    int i=0;
                    while( i<celdasorder.size()){
                        i++;
                        if (celdasorder[i-1]==m){
                            centrosall.push_back(i-1+floor(macrocell[m-1]/2));
                            distancetocenter.push_back(abs(direction-centrosall[m-1]));
                            m=m+1;
                            i=0;
                        }
                    }
                    int cellnumber=std::distance(distancetocenter.begin(),std::min_element(distancetocenter.begin(),distancetocenter.end()));
                    centro=centrosall[cellnumber];
                }
                std::cout<<centro<<std::endl;
            }
            //////MIRAR COMO HACER ESTOOO

            // si es la first vez activas el behavior para pasar a modo speed
            if (first==1){
                std::cout<< "ENTRO0000000000000000000000000000000000000"<<std::endl;
                behavior_msg.name="OBSTACLE_AVOIDANCE";

                msg.request.behavior=behavior_msg;
                ros::ServiceClient mode_service = ros_node.serviceClient<slamdunk_obstacle_avoidance_example::BehaviorSrv>("/drone5/activate_behavior");
                mode_service.call(msg);
                first=0;
                //Guardas a que punto de hacia adelante te dirijias y hacia que ventana
                scopex=valor[direction]+pose[0]-0.3;
                beforewin=centro;

            }

            if (keepgoingnewobs==1){
                //Guardas a que punto de hacia adelante te dirijias y hacia que ventana
                scopex=valor[direction]+pose[0]-0.3;
                keepgoingnewobs=0;
                beforewin=centro;

            }
            //Compruebo que si de una iteración a otra a cambiado mucho la direccion si es porque ha aparecido
            //un nuevo obstaculo hacia donde iba o no,entonces si no es por eso le digo que me mantenga
            //la direción en la que iba para asi evitar el cambio continuo.
            // Calculas que celdas mirar
            if (abs(beforewin-centro)>8 ){
                firstwin= beforewin-windowstocheck;
                lastwin=beforewin+windowstocheck;
                if (firstwin<0){
                    lastwin=lastwin+abs(firstwin);
                    firstwin=0;
                }
                if (lastwin>NUMBERWINDOWS-1){
                    firstwin=firstwin-(lastwin-NUMBERWINDOWS-1);
                    lastwin=NUMBERWINDOWS-1;
                }
                int cambiar=0;
                //compruebas las ventanas de alrededor y la propia si el porcentaje esta por encima del 35%
                for (unsigned short j =firstwin ; j <=lastwin ; j++){
                    if (porcentaje[j]>35){
                        cambiar=1;
                        break;
                    }
                }
                if (cambiar==1){
                    centro=beforewin;
                }
            }
            //guardas hacia cual ibas en la anterior
            beforewin=centro;
            std::cout<<centro<<std::endl;
            std::cout<<index<<std::endl;
            //calculas el centro de la ventana a la que te quieres ir
            int colsimage=index*centro+index/2;
            //int rowsimage=croppedImage.rows/2-1;
            //xyzrel[0] =-valor * std::tan((rowsimage - croppedImage.rows / 2) * m_settings.vFov / croppedImage.rows);
            //calculas la y de este punto// esto es cuestion de ajustar lo mejor posible
            //añadri el -0.1
        std::cout<<colsimage<<std::endl;
        xyzrel[1] =(valor[direction] *1.25* std::tan((colsimage - croppedImage.cols / 2) * m_settings.hFov / croppedImage.cols))-0.1;
        xyzrel[2]=0;
        //calculas la x de este punto es decir hacia adelante
        xyzrel[0]=valor[direction]+0.07;
        // En el caso de que haya obtaculo en las tres ventanas se va un poco mas a fuera de los extremos que ve la camara
        // hay que pensarlo mejor yo creo
        if (macrocell.empty()){
            std::cout<< "PAREDDDD:"<<std::endl;
            pared=1;
            if (beforewin<=NUMBERWINDOWS/2){
                xyzrel[1] =valor[direction] * std::tan(((0-(index/2)) - croppedImage.cols / 2) * m_settings.hFov / croppedImage.cols)-0.1;
                xyzrel[0]=valor[direction]/2+0.04;
            }
            else if (beforewin>NUMBERWINDOWS/2){
                xyzrel[1] =valor[direction] * std::tan(((croppedImage.cols+(index/2)) - croppedImage.cols / 2) * m_settings.hFov / croppedImage.cols)-0.1;
                xyzrel[0]=valor[direction]/2+0.04;
            }
            else{
                xyzrel[1] =0;
                xyzrel[0]=0;
            }
            if (std::isnan(valor[direction])){
                xyzrel[1] =1;
                xyzrel[0]=0;
            }


        }else

        std::cout<< "Go To y:"<< xyzrel[1] << "Go To x:"<< xyzrel[0] <<std::endl;
        // Convertir esta posicion a velocidades y a absolutas
        float module=sqrt(xyzrel[0]*xyzrel[0]+xyzrel[1]*xyzrel[1]);
        //        std::cout<<"MODULE"<<module<<std::endl;
        if (module==0){
            vxyzrel[0]=0;
            vxyzrel[1]=0;
            vxyzrel[2]=0;
        }
        else if(pared==1){
            vxyzrel[0]=(xyzrel[0]/module)*VELOCITY/8;
            vxyzrel[1]=(xyzrel[1]/module)*VELOCITY/8;
            vxyzrel[2]=xyzrel[2];
        }
        else{
            vxyzrel[0]=(xyzrel[0]/module)*VELOCITY;
            vxyzrel[1]=(xyzrel[1]/module)*VELOCITY;
            vxyzrel[2]=xyzrel[2];
        }
        pared=0;
        vxyzworld=Rot_body_to_world*vxyzrel;
//        vin.header.stamp = ros::Time::now();
//        vin.header.frame_id = "/base_link";
//        vin.vector.x = vxyzrel[0];
//        vin.vector.y =vxyzrel[1];
//        vin.vector.z = vxyzrel[2];
//        try{
//            listener.transformVector("/odom",ros::Time(0) ,vin, "/base_link",vout);
//        }
//        catch (tf::TransformException ex)
//        {
//            ROS_ERROR("%s",ex.what());
//            ros::Duration(1.0).sleep();
//        }
//        vxyzworld[0]   =  vout.vector.x;
//        vxyzworld[1]   =  vout.vector.y;
//        vxyzworld[2]   = -vout.vector.z;

        //porcentajeselect.clear();
        porcentaje.clear();
        valor.clear();
        celdasorder.clear();
        macrocell.clear();
        eliminar.clear();
        centrosall.clear();
        distancetocenter.clear();
        //std::cout<< "Speed Y:"<< vxyzworld[1]<< "Speed X :"<< vxyzworld[0] <<std::endl;
        return vxyzworld;
        // Devolver las velocidades


    }// en el caso de que ya haya dejado de detectar obtaculo en la direccion que se esta moviendo
    // entonces sigue moviendose en este sentido hasta que alcance el punto que tenia como objetivo
    // en el caso de que encuentro otro obstaculo por el camino entonces volvera a entrar al if de arriba
    else if((obstacle==0) && first==0){
        std::cout<< "Actual:"<<pose[0] << "deseada:"<< scopex <<std::endl;
        if (pose[0]<scopex && keepgoingnewobs==0){
            keepgoingnewobs=1;
            //porcentajeselect.clear();
            porcentaje.clear();
            valor.clear();
            celdasorder.clear();
            centrosall.clear();
            distancetocenter.clear();
            macrocell.clear();
            eliminar.clear();
            return vxyzworld;
            // Devolver las velocidades

        }
        else if (pose[0]<scopex){
            //porcentajeselect.clear();
            porcentaje.clear();
            valor.clear();
            celdasorder.clear();
            macrocell.clear();
            centrosall.clear();
            distancetocenter.clear();
            eliminar.clear();
            return vxyzworld;
            // Devolver las velocidades


        }
        // una vez que lo ha alcanzado pasa a desactivar el modo speed por eso finish=1
        else{
            finish=1;
            first=1;
        }



    }
    // En el caso de que haya alcanzado el punto objetivo entonces desactiva el modo speed y le manda el punto a donde estaba yendo
    // en este caso lo he puesto yo a mano pero esto es rapido de cambiarlo para que lo cojo de forma autonoma entonces eso ya lo cambiare
    else if((obstacle==0) && finish==1){
        finish=0;
        keepgoingnewobs=0;
        //stop
        std::cout<< "SALIIIIO0000000000000000000000000000000000000"<<std::endl;
        velocitytosend.dx=0;
        velocitytosend.dy=0;
        velocitytosend.dz=0;
        pub_reference_speed_world.publish(velocitytosend);
        behavior_msg2.name="OBSTACLE_AVOIDANCE";
        first=1;
        msg2.request.behavior=behavior_msg2;
        ros::ServiceClient mode_service2 = ros_node.serviceClient<slamdunk_obstacle_avoidance_example::BehaviorSrv>("/drone5/inhibit_behavior");

        mode_service2.call(msg2);


//                std::vector<SimpleTrajectoryWaypoint> trajectory_waypoints_out;
//                int initial_checkpoint_out = 0;
//                bool is_periodic_out = false;
//                trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  0.0, 0.0, 1.3) );
                behavior_msg3.name="GO_TO_POINT";
                behavior_msg3.arguments="coordinates: [16,0.0,1.45]";

        msg3.request.behavior=behavior_msg3;
        ros::ServiceClient mode_service3 = ros_node.serviceClient<slamdunk_obstacle_avoidance_example::BehaviorSrv>("/drone5/activate_behavior");

        mode_service3.call(msg3);
//        vxyzworld[0]   =  0;
//        vxyzworld[1]   =  0;
//        vxyzworld[2]   = 0;

        // Devolver las velocidades
        //porcentajeselect.clear();
        porcentaje.clear();
        valor.clear();
        celdasorder.clear();
        macrocell.clear();
        centrosall.clear();
        distancetocenter.clear();
        eliminar.clear();
        return vxyzworld;

    }
    // Si no hay obtaculo y no entraen ninguno de los anteriores
    else{

        std::cout<< "NNO HAY obstaculo"<<finish<<std::endl;
        std::cout<< obstacle<<direction<<std::endl;
        //porcentajeselect.clear();
        porcentaje.clear();
        valor.clear();
        celdasorder.clear();
        centrosall.clear();
        distancetocenter.clear();
        macrocell.clear();
        eliminar.clear();
        //std::cout<< "Speed Y:"<< vxyzworld[1]<< "Speed X :"<< vxyzworld[0] <<std::endl;
        return vxyzworld;
    }
    //porcentajeselect.clear();
    porcentaje.clear();
    valor.clear();
    celdasorder.clear();
    macrocell.clear();
    centrosall.clear();
    distancetocenter.clear();
    eliminar.clear();




}
//void Avoid::buscar(int rows,int cols,int endr,int endc,const cv::Mat1f &img,std::array<float, 3> position, std::array<float, 3> orientation )
void Avoid::buscar(int rows,int cols,int endr,int endc,const cv::Mat1f &img,int windows,std::array<float, 3> speedref)
{

    float nozeroPoint=0.0;
    int ClosePoint=0;
    float ydirobsleft=0.0;
    float ydirobsright=0.0;
    float anguloleft=0.0;
    float anguloright=0.0;
    float angulodirdrone=0.0;
    float valuewindows=0.0;

    // recorres por filas y por columnas
    for (unsigned short j = rows; j <=endr ; j++)
    {
        for (unsigned short i = cols; i <=endc ; i++)
        {
            // obtienes la distancia que te da el depth
            xyzrel[2] = img.at<float>(j, i);
            //std::cout<< xyzrel[2]<<std::endl;
            // si son puntos nan los pones a cero
            if (std::isnan(xyzrel[2]))
            {
                xyzrel[2] = 0.0;
            }
            //nozeroPoint++;
            //cuentas cuantos puntos son distintos de 0
            if (xyzrel[2]>0){
                nozeroPoint++;



                // miras cuantos puntos estan a menos de el wall distance y mas de 0.8 y haces la media
                if (xyzrel[2] < m_settings.wallDetectionDistance && xyzrel[2] > 0.8){
                    ClosePoint++;
                    valuewindows=valuewindows+xyzrel[2];
                }
            }
        }

    }

    //std::cout<< valuewindows/ClosePoint<<std::endl;
    if (nozeroPoint==0)
    {
        std::cout<< "NNO HAY PUNTOOOOOOS"<<std::endl;
    }
    else{
        //Calculas el porcentaje de puntos por debajo del limite
        porcentaje.push_back((ClosePoint/nozeroPoint)*100);
    //    std::cout<< ClosePoint<<std::endl;
    //    std::cout<< nozeroPoint<<std::endl;
        // haces la media de la distancia
        if(ClosePoint==0){
           valor.push_back(1.85);
        }
        else{
        valor.push_back(valuewindows/ClosePoint);
        }

    }
    //calculas el valor de y del extremo horizontal de la izquierda de esta ventana() cambiar menos a real)
    //Poner el -0.1//
    ydirobsleft =-valor[windows] * std::tan((cols - img.cols / 2) * m_settings.hFov / img.cols)-0.1;
    // calculas el angulo al que esta este punto
    anguloleft=atan2(ydirobsleft,valor[windows]);
    //std::cout<<"DIR iz"<<anguloleft<<std::endl;
    //calculas el valor de y del extremo horizontal de la izquierda de esta ventana (cambiar menos a real)
    //Poner el -0.1//
    ydirobsright=-valor[windows] * std::tan((endc - img.cols / 2) * m_settings.hFov / img.cols)-0.1;
    // calculas el angulo al que esta este punto
    anguloright=atan2(ydirobsright,valor[windows]);
    //std::cout<<"DIR dr"<<anguloright<<std::endl;
    // calculas el angulo de la direccion a la que te dirijes
    angulodirdrone=atan2(speedref[1],speedref[0]);
    //std::cout<<"DIR DRON"<<angulodirdrone<<std::endl;
    // si la direccion a la que te dirijes esta en el rango de esta ventana entonces pones el index de la ventana que es.
    if (angulodirdrone>=anguloright && angulodirdrone<anguloleft){
        direction=windows;

    }

}



} // namespace episkopi
