/*
 * Copyright (c) 2016 Parrot S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the Parrot Company nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE PARROT COMPANY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "episkopi/Avoid.hpp"

#include "episkopi/AvoidSettings.hpp"

#include <Eigen/Geometry>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <sys/time.h>

//////PASOS A SEGUIR:::
/// 1. Determinar si el obtaculpo esta en la direccion que te mueves y sus celdas adyacentes (en función del tamaño del dron y de la division de celdas)
/// 2. Determinar si hay obtaculo en la direccion
/// 3. UNa vez detectado buscar macroceldas que quedan libres y de cuantas celdas son
/// 4.escoger la macro celda mas grande y que sea suficiente para que quepa el dron
/// 5. Determinar el centro de esta
/// 6. Tener en cuenta que si existen dos macro celdas muy parecidad que de una iteracción a otra mantenga la dirección en la que iba
///
///

namespace episkopi
{
//Recibir la posicion que queremos ir
//void Avoid::obstacleavoidance(std::array<float, 3> position, std::array<float, 3> orientation, const cv::Mat1f &img)
Eigen::Vector3f  Avoid::obstacleavoidance(const cv::Mat1f &img,ros::NodeHandle ros_node,std::array<float, 3> speedref,std::array<float, 3> pose)
{   
    // Setup a rectangle to define your region of interest
    cv::Rect myROI(0, CROPSTARTHEIGHT, img.cols, CROPHEIGHT);

    // Crop the full image to that image contained by the rectangle myROI
    cv::Mat croppedImage = img(myROI);
    obstacle=0;
    direction=0;
    valor=0;
    // number of windows es en cuantas ventanas quieres dividir
    // index sale el numero de pixeles por ventana
    index=croppedImage.cols/NUMBERWINDOWS;
    // recorres cada ventana
    for (unsigned short j = 0; j <NUMBERWINDOWS ; j++){

        buscar(0,j*index,croppedImage.rows-1,index*(j+1)-1,croppedImage,j,speedref);



    }

    // si hay obtaculo y esta en la direccion que te esta mociendo entra
    if (obstacle==1 && direction>0){
        // buscas cual es el porcentaje mas pequeño y te devuelve que ventana es esta desde 0 a 2 en el caso de 3 ventanas
        int free = std::distance(porcentaje.begin(),std::min_element(porcentaje.begin(),porcentaje.end()));


        // si es la first vez activas el behavior para pasar a modo speed
        if (first==1){
            std::cout<< "ENTRO0000000000000000000000000000000000000"<<std::endl;
            behavior_msg.name="OBSTACLE_AVOIDANCE";

            msg.request.behavior=behavior_msg;
            ros::ServiceClient mode_service = ros_node.serviceClient<slamdunk_obstacle_avoidance_example::BehaviorSrv>("/drone4/activate_behavior");
            mode_service.call(msg);
            first=0;
            //Guardas a que punto de hacia adelante te dirijias y hacia que ventana
            scopex=valor+pose[0]; 
            beforewin=free;

        }

        if (keepgoingnewobs==1){
            //Guardas a que punto de hacia adelante te dirijias y hacia que ventana
            scopex=valor+pose[0];
            keepgoingnewobs=0;
            beforewin=free;

        }
        //std::cout<< "porcentajeselect :"<< porcentajeselect <<std::endl;
        //Guardas el vector porcentaje en otra variable
        porcentajeselect=porcentaje;
        // borras el porcentaje de la ventana hacia a la que te diirijas y te ibas a chocar
        porcentajeselect.erase(porcentajeselect.begin()+direction-1);
        //entre las resultantes miras si son muy parecidas.
        //En este caso solo hay dos pero si hubiese mas habria que modificar esto
        if (abs(porcentajeselect[0]-porcentajeselect[1])<10){
            //Si son muy parecidas y te dirijias hacia la 1 y te ibas a chocar
            //que en el caso de direction significa la windows 0 entonces te vas hacia el estremo contrario
            if (direction==1){
                free=2;
            }
            //Si son muy parecidas y te dirijias hacia la 2 y te ibas a chocar
            //que en el caso de direction significa la windows 1(centro) entonces miras hacia que sentido te estabas moviendo en el anterior
            else if(direction==2){
                // dependiendo el sentido que te estabas moviendo entonces lo mantienes para que no este cambiandose todo el rato.
                if (beforewin==0){
                   free=0;
                }else if(beforewin==2){
                   free=2;
                }
            }
            else if(direction==3){
                free=0;
            }
        }

        std::cout<< "El menor porcentaje :"<< free <<std::endl;
        //guardas hacia cual ibas en la anterior
        beforewin=free;
        //calculas el centro de la ventana a la que te quieres ir
        int colsimage=index*free+index/2;
        int rowsimage=croppedImage.rows/2-1;
        //xyzrel[0] =-valor * std::tan((rowsimage - croppedImage.rows / 2) * m_settings.vFov / croppedImage.rows);
        //calculas la y de este punto// esto es cuestion de ajustar lo mejor posible
        xyzrel[1] =(-valor *1.05* std::tan((colsimage - croppedImage.cols / 2) * m_settings.hFov / croppedImage.cols))-0.1;
        xyzrel[2]=0;
        //calculas la x de este punto es decir hacia adelante
        xyzrel[0]=valor+0.07;
        // En el caso de que haya obtaculo en las tres ventanas se va un poco mas a fuera de los extremos que ve la camara
        // hay que pensarlo mejor yo creo
        if (porcentaje[0]>50 && porcentaje[1]>50 && porcentaje[2]>50){
            std::cout<< "PAREDDDD:"<<std::endl;
            if (direction==1){
                xyzrel[1] =-valor * std::tan(((0-(index/2)) - croppedImage.cols / 2) * m_settings.hFov / croppedImage.cols)-0.1;
                xyzrel[0]=valor/2+0.04;
            }
            else if (direction==2){
                xyzrel[1] =-valor * std::tan(((0-(index/2)) - croppedImage.cols / 2) * m_settings.hFov / croppedImage.cols)-0.1;
                xyzrel[0]=valor/2+0.04;
            }
            else if (direction==3){
                xyzrel[1] =-valor * std::tan(((croppedImage.cols+(index/2)) - croppedImage.cols / 2) * m_settings.hFov / croppedImage.cols)-0.1;
                xyzrel[0]=valor/2+0.04;
            }
            else{
                xyzrel[1] =0;
                xyzrel[0]=0;
            }



        }

        std::cout<< "Go To y:"<< xyzrel[1] << "Go To x:"<< xyzrel[0] <<std::endl;
        // Convertir esta posicion a velocidades y a absolutas
        float module=sqrt(xyzrel[0]*xyzrel[0]+xyzrel[1]*xyzrel[1]);
        //        std::cout<<"MODULE"<<module<<std::endl;
        vxyzrel[0]=(xyzrel[0]/module)*VELOCITY;
        vxyzrel[1]=(xyzrel[1]/module)*VELOCITY;
        vxyzrel[2]=xyzrel[2];

        vin.header.stamp = ros::Time::now();
        vin.header.frame_id = "/base_link";
        vin.vector.x = vxyzrel[0];
        vin.vector.y =vxyzrel[1];
        vin.vector.z = vxyzrel[2];
        try{
            listener.transformVector("/odom",ros::Time(0) ,vin, "/base_link",vout);
        }
        catch (tf::TransformException ex)
        {
            ROS_ERROR("%s",ex.what());
            ros::Duration(1.0).sleep();
        }
        vxyzworld[0]   =  vout.vector.x;
        vxyzworld[1]   =  vout.vector.y;
        vxyzworld[2]   = -vout.vector.z;
        porcentajeselect.clear();
        porcentaje.clear();
        //std::cout<< "Speed Y:"<< vxyzworld[1]<< "Speed X :"<< vxyzworld[0] <<std::endl;
        return vxyzworld;
        // Devolver las velocidades


    }// en el caso de que ya haya dejado de detectar obtaculo en la direccion que se esta moviendo
    // entonces sigue moviendose en este sentido hasta que alcance el punto que tenia como objetivo
    // en el caso de que encuentro otro obstaculo por el camino entonces volvera a entrar al if de arriba
    else if((obstacle==0) && first==0){
        std::cout<< "Actual:"<<pose[0] << "deseada:"<< scopex <<std::endl;
        if (pose[0]<scopex && keepgoingnewobs==0){
            keepgoingnewobs=1;
            porcentajeselect.clear();
            porcentaje.clear();
            return vxyzworld;
            // Devolver las velocidades

        }
        else if (pose[0]<scopex){
            porcentajeselect.clear();
            porcentaje.clear();
            return vxyzworld;
            // Devolver las velocidades


        }
        // una vez que lo ha alcanzado pasa a desactivar el modo speed por eso finish=1
        else{
            finish=1;
            first=1;
        }



    }
    // En el caso de que haya alcanzado el punto objetivo entonces desactiva el modo speed y le manda el punto a donde estaba yendo
    // en este caso lo he puesto yo a mano pero esto es rapido de cambiarlo para que lo cojo de forma autonoma entonces eso ya lo cambiare
    else if((obstacle==0) && finish==1){
        finish=0;
        keepgoingnewobs=0;
        //stop
        std::cout<< "SALIIIIO0000000000000000000000000000000000000"<<std::endl;
        behavior_msg2.name="OBSTACLE_AVOIDANCE";

        msg2.request.behavior=behavior_msg2;
        ros::ServiceClient mode_service2 = ros_node.serviceClient<slamdunk_obstacle_avoidance_example::BehaviorSrv>("/drone4/inhibit_behavior");

        mode_service2.call(msg2);


        //        std::vector<SimpleTrajectoryWaypoint> trajectory_waypoints_out;
        //        int initial_checkpoint_out = 0;
        //        bool is_periodic_out = false;
        //        trajectory_waypoints_out.push_back( SimpleTrajectoryWaypoint(  0.0, 0.0, 1.3) );
        //        behavior_msg3.name="GO_TO_POINT";
        //        behavior_msg3.arguments="coordinates: [3.5,0.0,1.3]";

        msg3.request.behavior=behavior_msg3;
        ros::ServiceClient mode_service3 = ros_node.serviceClient<slamdunk_obstacle_avoidance_example::BehaviorSrv>("/drone4/activate_behavior");

        mode_service3.call(msg3);
        vxyzworld[0]   =  0;
        vxyzworld[1]   =  0;
        vxyzworld[2]   = 0;

        // Devolver las velocidades
        porcentajeselect.clear();
        porcentaje.clear();
        return vxyzworld;

    }
    // Si no hay obtaculo y no entraen ninguno de los anteriores
    else{

        std::cout<< "NNO HAY obstaculo"<<finish<<std::endl;
        std::cout<< obstacle<<direction<<std::endl;
        porcentajeselect.clear();
        porcentaje.clear();
        //std::cout<< "Speed Y:"<< vxyzworld[1]<< "Speed X :"<< vxyzworld[0] <<std::endl;
        return vxyzworld;
    }
    porcentajeselect.clear();
    porcentaje.clear();




}
//void Avoid::buscar(int rows,int cols,int endr,int endc,const cv::Mat1f &img,std::array<float, 3> position, std::array<float, 3> orientation )
void Avoid::buscar(int rows,int cols,int endr,int endc,const cv::Mat1f &img,int windows,std::array<float, 3> speedref)
{

    float nozeroPoint=0.0;
    int ClosePoint=0;
    float ydirobsleft=0.0;
    float ydirobsright=0.0;
    float anguloleft=0.0;
    float anguloright=0.0;
    float angulodirdrone=0.0;
    float valuewindows=0.0;

    // recorres por filas y por columnas
    for (unsigned short j = rows; j <=endr ; j++)
    {
        for (unsigned short i = cols; i <=endc ; i++)
        {
            // obtienes la distancia que te da el depth
            xyzrel[2] = img.at<float>(j, i);
            // si son puntos nan los pones a cero
            if (std::isnan(xyzrel[2]))
            {
                xyzrel[2] = 0.;
            }

            //cuentas cuantos puntos son distintos de 0
            if (xyzrel[2]>0){
                nozeroPoint++;



                // miras cuantos puntos estan a menos de el wall distance y mas de 0.8 y haces la media
                if (xyzrel[2] < m_settings.wallDetectionDistance && xyzrel[2] > 0.8){
                    ClosePoint++;
                    valuewindows=valuewindows+xyzrel[2];
                }
            }
        }

    }

    //std::cout<< valuewindows/ClosePoint<<std::endl;
    if (nozeroPoint==0)
    {
        std::cout<< "NNO HAY PUNTOOOOOOS"<<std::endl;
    }
    else{
        //Calculas el porcentaje de puntos por debajo del limite
        porcentaje.push_back((ClosePoint/nozeroPoint)*100);
    }
    // si el porcentaje esta por debajo del 35
    if (porcentaje[windows] >35){
        //activas la variable diciendo que hay obstaculo

        obstacle=1;
        // haces la media de la distancia
        valor=valuewindows/ClosePoint;
        //std::cout<<"windows"<<windows<<std::endl;
        //calculas el valor de y del extremo horizontal de la izquierda de esta ventana
        ydirobsleft =-valor * std::tan((cols - img.cols / 2) * m_settings.hFov / img.cols)-0.1;
        // calculas el angulo al que esta este punto
        anguloleft=atan2(ydirobsleft,valor);
        //std::cout<<"DIR iz"<<anguloleft<<std::endl;
        //calculas el valor de y del extremo horizontal de la izquierda de esta ventana
        ydirobsright=-valor * std::tan((endc - img.cols / 2) * m_settings.hFov / img.cols)-0.1;
        // calculas el angulo al que esta este punto
        anguloright=atan2(ydirobsright,valor);
        //std::cout<<"DIR dr"<<anguloright<<std::endl;
        // calculas el angulo de la direccion a la que te dirijes
        angulodirdrone=atan2(speedref[1],speedref[0]);
        //std::cout<<"DIR DRON"<<angulodirdrone<<std::endl;
        // si la direccion a la que te dirijes esta en el rango de esta ventana entonces pones el index de la ventana que es. El mas 1 para no ser cero la de la izquierda.
        if (angulodirdrone>anguloright && angulodirdrone<anguloleft){
            direction=windows+1;
        }
        //std::cout<< "OBSTACLE WINDOWS :"<< j  << "Porxcentaje"<< porcentaje[j] <<std::endl;

    }
}



} // namespace episkopi
