/*
 * Copyright (c) 2016 Parrot S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the Parrot Company nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE PARROT COMPANY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef EPISKOPI_AVOID_HPP
#define EPISKOPI_AVOID_HPP

#include "AvoidSettings.hpp"
#include "tf/transform_listener.h"
//#include <geometry_msgs/Vector3Stamped.h>
#include <ros/ros.h>
#include <Eigen/Geometry>
//#include "tf/transform_datatypes.h"
//#include "tf/transform_broadcaster.h"
#include <geometry_msgs/TransformStamped.h>
#include "slamdunk_obstacle_avoidance_example/BehaviorEvent.h"
#include "slamdunk_obstacle_avoidance_example/BehaviorCommand.h"
#include "slamdunk_obstacle_avoidance_example/BehaviorSrv.h"
#include "slamdunk_obstacle_avoidance_example/droneCommand.h"
#include "slamdunk_obstacle_avoidance_example/droneSpeeds.h"
//#include "slamdunk_obstacle_avoidance_example/BehaviorSrvResponse.h"
// Aerostack msgs
#include <array>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


namespace episkopi
{

class Avoid
{


private:
    /// computation of negative forces
//    void obstacleavoidance(std::array<float, 3> position,std::array<float, 3> orientation,const cv::Mat1f &image);
//    void buscar(int rows,int cols,const cv::Mat1f &img,std::array<float, 3> position,std::array<float, 3> orientation);


private:
    /// algorithm settings
    AvoidSettings m_settings;
    ros::Publisher pub_reference_speed_world;
    geometry_msgs::Vector3Stamped vin, vout;
    tf::TransformListener listener;
    typedef Eigen::Matrix<float,3,3> Mat3x3;
    /// the drone coordinate system is set as follow :
    /// x axis is upward
    /// y axis is rightward
    /// z axis is toward
    int obstacle;
    const float NUMBERWINDOWS =48.0;
    const int CROPHEIGHT=15;
    const int CROPSTARTHEIGHT=30;
    int index=0;
    int direction=0;
    int first=1;
    int keepgoingnewobs=0;
    int finish=0;
    const float VELOCITY=0.4;
    const float STANDARD_WIDTH=3.4;
    const float STANDARD_DISTANCE=1.7;
    const float DRONE_WIDTH=0.66;
    int pared=0;
    float scopex;
    std::vector<float> valor;
    std::vector<float> porcentajeselect;
    std::vector<int> centrosall;
    std::vector<float> distancetocenter;
    int beforewin;
    slamdunk_obstacle_avoidance_example::droneSpeeds velocitytosend;
    slamdunk_obstacle_avoidance_example::BehaviorCommand behavior_msg;
    slamdunk_obstacle_avoidance_example::BehaviorCommand behavior_msg3;
    slamdunk_obstacle_avoidance_example::BehaviorSrv msg;
    slamdunk_obstacle_avoidance_example::BehaviorSrv msg3;
    slamdunk_obstacle_avoidance_example::BehaviorCommand behavior_msg2;
    slamdunk_obstacle_avoidance_example::BehaviorSrv msg2;
    std::vector<float> porcentaje;
    std::vector<int> celdasorder;
    std::vector<int> macrocell;
    std::vector<int> eliminar;
    Eigen::Vector3f xyzrel = Eigen::Vector3f(0., 0., 0.);
    Eigen::Vector3f vxyzrel = Eigen::Vector3f(0., 0., 0.);
    Eigen::Vector3f vxyzworld = Eigen::Vector3f(0., 0., 0.);
    Eigen::Vector3f m_futurePos = Eigen::Vector3f(0., 0., 0.);

    /// Output speed of the drone, in body referential
    Eigen::Vector3f m_speedOut = Eigen::Vector3f(0., 0., 0.);

    /// Input speed of the drone, in body referential
    Eigen::Vector3f m_speedIn = Eigen::Vector3f(0., 0., 0.);

    /// Rotation of the drone on the Z (roll), Y (pitch)
    /// and X (yaw) axis in [°]
    Eigen::Vector3f m_angles = Eigen::Vector3f(0., 0., 0.);

    /// repulsive force computed from the voxels which have
    /// an impact time < m_settings.impactTimeThreshold
    Eigen::Vector3f m_forceVoxelWithImpact = Eigen::Vector3f (0., 0., 0.);

    /// repulsive force computed from the voxels which have
    /// an impact time > m_settings.impactTimeThreshold
    Eigen::Vector3f m_forceVoxelWithoutImpact = Eigen::Vector3f (0., 0., 0.);

    /// Attractive forces used for the FlightPlan mode
    Eigen::Vector3f m_attractiveForce = Eigen::Vector3f (0., 0., 0.);

    float m_minImpactTime = 0;

    bool m_stopWall = false; ///< wall detection parameter
    std::array<bool, 3> m_avoidingXYZ = {{false, false, false}};
public:
   Eigen::Vector3f obstacleavoidance(const cv::Mat1f &image,ros::NodeHandle ros_node,std::array<float, 3> speedref,std::array<float, 3> position, Mat3x3 matriz);
   void buscar(int rows,int cols,int endrows, int endcols,const cv::Mat1f &img, int windows,std::array<float, 3> speedref2);

};

} // namespace episkopi

#endif // EPISKOPI_AVOID_HPP
