/*
 * Copyright (c) 2016 Parrot S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the Parrot Company nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE PARROT COMPANY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "obstacle_avoidance_nodelet.hpp"

#include <array>
#include <cv_bridge/cv_bridge.h>

void ObstacleAvoidanceNodelet::onInit()
{
    ROS_DEBUG("Obstacle avoidance nodelet initialization");

    m_nh = getNodeHandle();

    m_depthSub =
        m_nh.subscribe("/depth_map/image", 1,&ObstacleAvoidanceNodelet::onNewDepthMapImage, this);
    m_speedrefSub =
        m_nh.subscribe("/drone5/EstimatedSpeed_droneGMR_wrt_GFF", 1,&ObstacleAvoidanceNodelet::speedrefCb, this);

 //   m_attitudeChangedSub = m_nh.subscribe("/drone4/states/ardrone3/PilotingState/AltitudeChanged", 1,&ObstacleAvoidanceNodelet::attitudeChangedCb, this);

 //   m_speedChangedSub = m_nh.subscribe("/drone4/states/ardrone3/PilotingState/SpeedChanged", 1,&ObstacleAvoidanceNodelet::speedChangedCb, this);

 //   m_pilotingVelocitySub = m_nh.subscribe("/drone4/cmd_vel", 1, &ObstacleAvoidanceNodelet::pilotingVelocityCb, this);//¿¿¿UTIL???

    m_pose = m_nh.subscribe("/drone5/EstimatedPose_droneGMR_wrt_GFF", 1, &ObstacleAvoidanceNodelet::poseCb, this);// MIRAR SI ES EL TOPIC

    //Leer la posicion a la que queremos ir

//    m_avoidPilotingVelocityPub = m_nh.advertise<geometry_msgs::Twist>("pilotingVelocityWithAvoidance", 1);
    pub_reference_speed_world = m_nh.advertise<slamdunk_obstacle_avoidance_example::droneSpeeds>("/drone5/droneSpeedsRefs", 1);

}

void ObstacleAvoidanceNodelet::onNewDepthMapImage(
    const sensor_msgs::ImageConstPtr &imageMsg)
{
    ROS_DEBUG("New depth map");
    cv_bridge::CvImageConstPtr img_bridge;
    img_bridge = cv_bridge::toCvShare(imageMsg,
                                      sensor_msgs::image_encodings::TYPE_32FC1);
     Eigen::Vector3f sendspeed = Eigen::Vector3f(0., 0., 0.);
    m_speedref = {{static_cast<float>(0.5), static_cast<float>(-0.3),static_cast<float>(0)}};
    //std::cout<<"entra"<<std::endl;
    //Meter a la pose que queremos ir
   // m_avoid.obstacleavoidance(m_pose_position,m_pose_orientation,img_bridge->image);
      sendspeed=m_avoid.obstacleavoidance(img_bridge->image,m_nh,m_speedref,pose,Rot_body_to_world);
      velocitytosend.dx=sendspeed[0];
      velocitytosend.dy=sendspeed[1];
      velocitytosend.dz=0;
      std::cout<< "Speed Y:"<< velocitytosend.dy<< "Speed X :"<< velocitytosend.dx <<std::endl;
      pub_reference_speed_world.publish(velocitytosend);
     //se obtendrian las velocidades y se pasand al comand
     //       geometry_msgs::Twist pilotingCommandMsg;
     //       pilotingCommandMsg.linear.x = m_droneCMD[1];
     //       pilotingCommandMsg.linear.y = m_droneCMD[0];
     //      pilotingCommandMsg.linear.z = m_droneCMD[2];
            //pilotingCommandMsg.angular.z = msg->angular.z;
     //       m_avoidPilotingVelocityPub.publish(pilotingCommandMsg);

}
//Leer la posicion a la que vamoss
void ObstacleAvoidanceNodelet::speedrefCb(
    const slamdunk_obstacle_avoidance_example::droneSpeeds &msg)
{
    m_speedref = {{static_cast<float>(msg.dx), static_cast<float>(msg.dy),
                   static_cast<float>(msg.dz)}};
}
//void ObstacleAvoidanceNodelet::attitudeChangedCb(
//    const geometry_msgs::Vector3 &msg)
//{
//    m_attitude = {{static_cast<float>(msg.x), static_cast<float>(msg.y),
//                   static_cast<float>(msg.z)}};
//}
void ObstacleAvoidanceNodelet::poseCb(const slamdunk_obstacle_avoidance_example::dronePose &msg)
{
    pose = {{static_cast<float>(msg.x), static_cast<float>(msg.y),
                   static_cast<float>(msg.z)}};
    Mat3x3 Rot_yaw;
    Mat3x3 Rot_pitch;
    Mat3x3 Rot_roll;


    Rot_yaw(0,0) = cos(msg.yaw);
    Rot_yaw(1,0) = sin(msg.yaw);
    Rot_yaw(2,0) = 0;
    Rot_yaw(0,1) = -sin(msg.yaw);
    Rot_yaw(1,1) = cos(msg.yaw);
    Rot_yaw(2,1) = 0;
    Rot_yaw(0,2) = 0;
    Rot_yaw(1,2) = 0;
    Rot_yaw(2,2) = 1;

    //    Rot_yaw << cos(yaw), -sin(yaw), 0,
    //               sin(yaw),  cos(yaw), 0,
    //                 0     ,    0     , 1;

    Rot_pitch(0,0) = cos(msg.pitch);
    Rot_pitch(1,0) = 0;
    Rot_pitch(2,0) = -sin(msg.pitch);
    Rot_pitch(0,1) = 0;
    Rot_pitch(1,1) = 1;
    Rot_pitch(2,1) = 0;
    Rot_pitch(0,2) = sin(msg.pitch);
    Rot_pitch(1,2) = 0;
    Rot_pitch(2,2) = cos(msg.pitch);


    //    Rot_pitch << cos(pitch), 0  , sin(pitch),
    //                    0      , 1  ,    0,
    //                 -sin(pitch),0  , cos(pitch);

    Rot_roll(0,0) = 1;
    Rot_roll(1,0) = 0;
    Rot_roll(2,0) = 0;
    Rot_roll(0,1) = 0;
    Rot_roll(1,1) = cos(msg.roll);
    Rot_roll(2,1) = sin(msg.roll);
    Rot_roll(0,2) = 0;
    Rot_roll(1,2) = -sin(msg.roll);
    Rot_roll(2,2) = cos(msg.roll);


    //    Rot_roll << 1,    0     ,    0,
    //                0, cos(roll),-sin(roll),
    //                0, sin(roll), cos(roll);

    Rot_body_to_world = Rot_yaw*Rot_pitch*Rot_roll; // ZYX notation (yaw*pitch*roll)

}

//void ObstacleAvoidanceNodelet::speedChangedCb(const geometry_msgs::Vector3 &msg)
//{
//    m_speed = {{static_cast<float>(msg.x), static_cast<float>(msg.y),
//                static_cast<float>(msg.z)}};
//}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(ObstacleAvoidanceNodelet, nodelet::Nodelet);
